import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Card, Button } from 'react-bootstrap'

const Calcas = () => {
    const [roupas, setRoupas] = useState([]);

    function getRoupas() {
        axios.get('http://localhost:8888/api/V1/categories/2').then(res => {
            let data = res.data
            setRoupas(data.items)
        })
    }

    let cardStyle = {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: "center",
        alignItems: "center",
        margin:"15px"
    }

    useEffect(()=>{
        getRoupas();
    }, [])

    return (
        <div className="h-100 mx-auto p-2 float-right" style={{width: "90%"}}>
            <h2 className="text-danger">Calças</h2><br/>
            <hr/>
            <div style={cardStyle}>
                {roupas.map((calca, idx) => (
                    <Card key={idx} style={{ width: '13rem', margin: '15px', textAlign:"center" }} >
                        <Card.Img variant="top" src={require(`./${calca.image}`)} className="img-thumbnail" style={{height: "250px"}} />
                        <Card.Title>{calca.name}</Card.Title>
                        <Card.Subtitle>{"R$".concat(calca.price).replace(".", ",")}</Card.Subtitle>
                        <Button variant="danger">Comprar</Button>
                    </Card>
                ))}
            </div>
        </div>
    )
}

export default Calcas;