import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Card, Button } from 'react-bootstrap'
import { connect } from 'react-redux'

const Camisetas = (props) => {
    const [camisetas, setcamisetas] = useState([]);

    function getCamisetas() {
        axios.get('http://localhost:8888/api/V1/categories/1').then(res => {
            let data = res.data
            setcamisetas(data.items)
        })
    }

    useEffect(()=>{
        getCamisetas();
    }, [])
   
    let cardStyle = {
        display: 'flex',
        flexWrap: 'wrap',
        padding: '10px',
        justifyContent: "center",
        alignItems: "center"
    }

    return (
        <div className="h-100 mx-auto p-2 float-right" style={{width: "90%"}}>
            <h2 className="text-danger">Camisetas</h2><br/>
            <hr/>
        <div style={cardStyle}>
                {camisetas.map((camiseta, idx) => (
                    <Card key={idx} style={{ width: '15rem', margin: '15px', textAlign:"center" }}>
                        <Card.Img variant="top" src={require(`./${camiseta.image}`)} className="img-thumbnail h-50"/>
                        <Card.Title>{camiseta.name}</Card.Title>
                        <Card.Subtitle>{"R$".concat(camiseta.price).replace(".", ",")}</Card.Subtitle>
                        <Button variant="danger">Comprar</Button>
                    </Card>
                ))}
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    filter: state.categorie.categoria_filter
})

export default connect(mapStateToProps, null)(Camisetas);