import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Card, Button } from 'react-bootstrap'
import { connect } from 'react-redux'

const Sapatos = (props) => {
    
    const [sapatos, setSapatos] = useState([]);

    function getSapatos() {
        axios.get('http://localhost:8888/api/V1/categories/3').then(res => {
            let data = res.data
            setSapatos(data.items)
        })
    }

    useEffect(()=>{
        getSapatos();
    })

    let cardStyle = {
        display: 'flex',
        flexWrap: 'wrap',
        padding: '10px',
        justifyContent: "center",
        alignItems: "center"
    }

    let filter = props.filter

    return (
        <div className="h-100 mx-auto p-2 float-right" style={{width: "90%"}}>
            <h2 className="text-danger">Sapatos</h2><br/>
            <hr/>
            <div style={cardStyle}>
                {sapatos.map((sapato, idx) => (
                    <Card key={idx} style={{ width: '13rem', margin: '15px', textAlign:"center" }}>
                        <Card.Img variant="top" src={require(`./${sapato.image}`)} className="img-thumbnail" style={{height: "200px"}}/>
                        <Card.Title>{sapato.name}</Card.Title>
                        <Card.Subtitle>{"R$".concat(sapato.price).replace(".", ",")}</Card.Subtitle>
                        <Button variant="danger">Comprar</Button>
                    </Card>
                ))}
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    filter: state.categorie.categoria_filter
})

export default connect(mapStateToProps, null)(Sapatos);