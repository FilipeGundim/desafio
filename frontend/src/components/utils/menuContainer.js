import React from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import BreadCrumb from './breadCrumb'

const MenuContainer = () => {
    
    return (
        <div style={{height: "130px"}}>
            <div className="bg-danger h-50 p-2">
                <Container className="bg-danger  w-100 text-light">
                    <Row>
                        <Col>Pagina Inicial</Col>
                        <Col>Camisetas</Col>
                        <Col>Calças</Col>
                        <Col>Sapatos</Col>
                        <Col>Contato</Col>
                    </Row>
                </Container>
            </div>
            <BreadCrumb />
        </div>
    )
}

export default MenuContainer;