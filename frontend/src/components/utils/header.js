import React from 'react';
import { Image, Button, Form, FormControl } from 'react-bootstrap'

import logo from '../categorias/media/logo.png'

const Header = () => {
    return (
        <div style={{height:"150px", justifyContent: "center"}}>
            <aside className="float-left w-50">
                <Image src={logo} fluid className="ml-4"style={{height:"140px"}}/>
            </aside>
            <aside className="float-right w-25 mr-1 mt-3">
                <Form inline >
                    <FormControl type="text"  />
                    <Button variant="outline-success" >Buscar</Button>
                </Form>
            </aside >
        </div>
    )
}

export default Header;