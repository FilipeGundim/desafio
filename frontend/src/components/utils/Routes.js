import React from 'react'
import { Redirect, Switch, Route } from 'react-router'
import Calca from '../categorias/calcas'
import Sapato from '../categorias/sapatos'
import Camiseta from '../categorias/camisetas'

const Router = () => {
    return (
        <Switch>
            <Route exact path="/" component={Sapato} />
            <Route path="/calcas" component={Calca} />
            <Route path="/sapatos" component={Sapato} />
            <Route path="/camisetas" component={Camiseta} />
            <Redirect fro='*' to='/' />
        </Switch>
    )
}

export default Router;