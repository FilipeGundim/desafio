import React from 'react'
import { Link } from 'react-router-dom'
import { ListGroup } from 'react-bootstrap'
import { changeBreadcrumb, categorieFilter } from '../../redux/actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

const ListOptions = (props) => {

    return (
        <div className="float-left m-2 border-right-0 text-center" style={{ width: "8%" }}>
            <p className="text-uppercase text-danger h4">Filtre por</p><br />
            <p className="text-uppercase text-info h5">Categorias</p>
            <ListGroup >
                <ListGroup.Item className="border-0" onClick={() => { props.changeBreadcrumb("Calças") }}><Link to='/calcas'> Roupas</Link></ListGroup.Item>
                <ListGroup.Item className="border-0" onClick={() => { props.changeBreadcrumb("Sapatos") }}><Link to='/sapatos'> Sapatos</Link></ListGroup.Item>
                <ListGroup.Item className="border-0" onClick={() => { props.changeBreadcrumb("Camisetas") }}><Link to='/camisetas'> Acessorios</Link></ListGroup.Item>
            </ListGroup>
            <br />
            <p className="text-uppercase text-info h5">Cores</p><br />
            <ListGroup style={{ display: "inline-block" }}>
                <ListGroup.Item className="bg-danger" onClick={() => { props.categorieFilter("Vermelha") }} ></ListGroup.Item>
                <ListGroup.Item className="bg-warning" onClick={() => { props.categorieFilter("Laranja") }} ></ListGroup.Item>
                <ListGroup.Item className="bg-info" onClick={() => { props.categorieFilter("Azul") }}></ListGroup.Item>
            </ListGroup>
            <p className="text-uppercase text-info h5">Tipo</p>
            <ListGroup >
                <ListGroup.Item className="border-0" ><input type="checkbox" /> Corrida</ListGroup.Item>
                <ListGroup.Item className="border-0" ><input type="checkbox" /> Caminhada</ListGroup.Item>
                <ListGroup.Item className="border-0" ><input type="checkbox" /> Casual</ListGroup.Item>
                <ListGroup.Item className="border-0" ><input type="checkbox" /> Social</ListGroup.Item>
            </ListGroup>
            <br />
        </div>
    )
}

function mapDispatchToProps(dispacth) {
    return bindActionCreators({ changeBreadcrumb, categorieFilter }, dispacth)
}

export default connect(null, mapDispatchToProps)(ListOptions);