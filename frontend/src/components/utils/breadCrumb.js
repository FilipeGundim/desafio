import React from 'react';
import { Breadcrumb } from 'react-bootstrap'
import { connect } from 'react-redux'

class BreadCrumb extends React.Component {

    render() {
        return (
            <Breadcrumb>
                <div className="ml-5">
                    {"Pagina inicial ->" + this.props.content}
                </div>
            </Breadcrumb>
        )
    }
}
const mapStateToProps = state => ({
    content: state.breadcrumb.breadcumber_content
})

export default connect(mapStateToProps, null)(BreadCrumb);