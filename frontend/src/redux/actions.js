export function changeBreadcrumb(e){
    return{
        type : 'CHANGE_BREADCRUMB',
        payload : e
    }
}

export function categorieFilter(e){
    return{
        type : 'CHANGE_CATEGORIE',
        payload: e
    }
}