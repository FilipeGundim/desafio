const INITIAL_STATE = {
    categoria_filter: ''
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'CHANGE_CATEGORIE':
            return { ...state, categoria_filter: action.payload }
        default:
            return { ...state }
    }
}