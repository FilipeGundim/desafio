const INITIAL_STATE = {
    breadcumber_content: ' Sapatos'
}

export default function(state=INITIAL_STATE, action){
    switch(action.type){
        case 'CHANGE_BREADCRUMB':
        return{...state, breadcumber_content: action.payload}
        default: return {...state}
    }
}