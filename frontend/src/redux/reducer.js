import { combineReducers } from 'redux'
import breadCrumbReducer from './reducers/breadCrumbReducer'
import categorieReducer from './reducers/categorieReducer'

const reducer = combineReducers({
    breadcrumb: breadCrumbReducer,
    categorie: categorieReducer
})

export default reducer;