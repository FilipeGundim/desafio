import React from 'react';
import Header from './components/utils/header'
import { BrowserRouter } from 'react-router-dom'
import Routes from './components/utils/Routes'
import MenuContainer from './components/utils/menuContainer'
import ListOptions from './components/utils/sideBar'
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <BrowserRouter>
      <Header />
      <MenuContainer />
      <div style={{width:"85%", marginLeft: "auto", marginRight:"auto"}}>
        <ListOptions />
        <Routes />
      </div>
    </BrowserRouter>
  )
}

export default App;